tfs=[180, 45, 39, 156, 54, 198, 177, 147, 27, 135, 99, 201, 228, 30, 210, 108, 225, 141, 78, 75, 120, 216, 57, 114, 135, 108, 78, 225, 210, 198, 156, 216, 27, 39, 75, 141, 45, 30, 54, 177, 114, 57, 99, 120, 201, 147, 180, 228, 78, 39, 120, 108] 
inp="00" # initial switch settings
z=0      # uniselector position (internal state)
while(True):
    print("\x1bcAshby Box:\n\n+----+\n| "+str(inp)+" |\n|    |\n| "+str("{0:b}".format(int(tfs[z])).zfill(8)[int(inp,2)*2])+str("{0:b}".format(int(tfs[z])).zfill(8)[int(inp,2)*2+1])+" |\n+----+")
    k=input("\nSwitches on top, lamps at the bottom.\nToggle switches [1] or [2] or [q]uit. ")
    inp=str(int(inp[0])^1)+inp[1] if k=="1" else inp[0]+str(int(inp[1])^1) if k=="2" else exit()
    z=(z+1)%len(tfs) # increment state
