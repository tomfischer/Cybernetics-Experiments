import random, copy, pylab

subjects = 200		# Anzahl simulierter Testpersonen
recursions = 7		# Anzahl der rekursiven Wiedereintritte jedes Subjekts
silben = 100				# Anzahl der Silben
reentry_reinforces = False    # Verfestigung bei Wiedereintritt ein/aus

AL=[]						# Sammlung aller Zeitserien (1 per Subjekt)
def silbenfabrik(sls):
    vokale = 'aeiou'
    konsonanten = 'bcdfghjklmnpqrstvwxyz'
    stack=[]
    for x in range(0, sls):  # Set von Nonsense-Silben generieren
        while True:  # keine doppelten Einträge generieren
            s = konsonanten[random.randint(0, len(konsonanten)-1)] + vokale[random.randint(0, len(vokale)-1)] + konsonanten[random.randint(0, len(konsonanten)-1)]
            if not s in stack: break
        stack.append(s)
    return stack

V = silbenfabrik(silben) # Array zur Speicherung der Nonsense-Silben

def erinnern(N, c):
    blankno = 0
    for m in N:
        if m == "*": blankno = blankno + 1   # Freie Träger ('*') zählen
    k = 0   # Erinnerungen vergessen:
    for m in N:
        if random.random() > 0.42: N[k] = '*'
        k += 1
    k=0     # Erinnerungen festigen:
    if reentry_reinforces:
        for m in N:
            if ('*' in N) and m != '*' and random.random() > 0.15:
                N[N.index('*')] = m   # Silbe in ersten freien Träger kopieren
            k = k + 1
    #print("Träger: " + str(N))
    # Anzahl behaltener Silben (leere Träger entfernt) speichern
    out.append(len(set(filter(lambda a: a != '*', N))))         
    c = c - 1               # herunterzählen
    if c > 0:             # Rekursion beenden?
        erinnern(N, c)  # rekursiver Wiedereintritt
    else:
        AL.append(out)  # diese Zeitserie an die Sammlung AL anhängen

for p in range(0, subjects): # Ein Durchlauf pro Subjekt
    out = [silben] # Unveränderte initiale Anzahl an Silben eintragen
    erinnern(copy.deepcopy(V), recursions) # Aufruf der Erinnerungsfunktion

AV=[]   # Array für Durchschnittswerte
for r in range(0, recursions+1):
    b = 0
    for p in range(0, subjects): b = b + AL[p][r]
    AV.append(b / subjects)

pylab.plot(AV, c='lightgray')
pylab.plot(AV, 'h', markersize=3.6, c='k') # Graph darstellen
pylab.xlim(-0.2, recursions+0.2); pylab.ylim(-2, silben+2)
pylab.title('Vergessenskurve')
pylab.xlabel('Wiedereintritte'); pylab.ylabel('Silben')
pylab.show()