import pylab
iterationen=50
gleichungen='''
N[i]=N[i-1]/2+1
'''
i=0; ertrag={}; fe=[]
for z in gleichungen.split('\n'): # Gleichung(en) vorbereiten:
    z=z.replace(' ', '')
    if len(z)>1 and z.index('=')>0:
        fe.append([z.split('[i]=')[0], z])
        ertrag[z.split('[i]=')[0]]=[0]*iterationen

def maschine(frm, i, iterationen, ertrag): # rekursive Maschine:    
    _c=0; i+=1; _d={}
    for _n in frm:
        formel=frm[_c][1]        
        for _o in ertrag: _d[_o]=ertrag[_o]
        _d['i']=i
        exec("from math import *;" + formel, _d)
        for _o in ertrag: ertrag[_o][i]=_d[_o][i]
        _c+=1
    return maschine(frm, i, iterationen, ertrag) if i<iterationen-1 else ertrag

for var in ertrag: # Startwert(e) abfragen:
    while True:
        try: ertrag[var][0]=(float(input("Startwert für " + var + " ? ")))   
        except ValueError: continue
        else: break 

ertrag=maschine(fe, i, iterationen, ertrag) # initialer Aufruf
pylab.gcf().canvas.set_window_title('Eigenform') # Visualisierung:
i=0; markers=['h','^','+','o','*','H']
for q in ertrag:
    pylab.plot(ertrag[q], c='lightgray')
    pylab.plot(ertrag[q], markers[i], markersize=3.6, c='k'); i+=1
pylab.show()
