from multiprocessing import Process, Value, Lock
import os, time, random

def metronom(kopplung, impulse, l):
    periode=.5
    impulsgewicht=.005
    time.sleep(random.uniform(0.2,4)) # initial wait
    while(True):
        b = impulse.value if kopplung.value else 0
        time.sleep(b * impulsgewicht + periode)
        os.system("aplay -q tick.wav &") # Bewegung nach links
        time.sleep(random.uniform(0, 0.03))
        with l: impulse.value += 1
        b = impulse.value if kopplung.value else 0
        time.sleep(b * -1* impulsgewicht + periode)
        os.system("aplay -q tock.wav &") # Bewegung nach rechts
        time.sleep(random.uniform(0 ,0.03))
        with l: impulse.value -= 1

kopplung = Value('b', False)
impulse = Value('i', 0)
l = Lock()
for n in range(0, 6): Process(target=metronom, args=(kopplung, impulse, l)).start()
while(True):
    print("Brett ist frei." if kopplung.value else "Brett ist fest.")
    input("Ändern per [Enter]-Taste.")
    kopplung.value = not kopplung.value