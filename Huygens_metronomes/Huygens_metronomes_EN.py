from multiprocessing import Process, Value, Lock
import os, time, random

def metronome(coupling, kicks, l):
    period=.5
    kickweight=.005
    time.sleep(random.uniform(0.2,4)) # initial wait
    while(True):
        b = kicks.value if coupling.value else 0
        time.sleep(b * kickweight + period)
        os.system("aplay -q tick.wav &") # left
        time.sleep(random.uniform(0, 0.03))
        with l: kicks.value += 1
        b = kicks.value if coupling.value else 0
        time.sleep(b * -1* kickweight + period)
        os.system("aplay -q tock.wav &") # right
        time.sleep(random.uniform(0 ,0.03))
        with l: kicks.value -= 1

coupling = Value('b', False)
kicks = Value('i', 0)
l = Lock()
for n in range(0, 6): Process(target=metronome, args=(coupling, kicks, l)).start()
while(True):
    print("Board can move." if coupling.value else "Board is fixed.")
    input("Press [Enter] to change.")
    coupling.value = not coupling.value