import pygame
    
breite, hoehe = 600, 400   # Fenster-Dimensionen
iterationen = 150 
rat_bereich = breite/350     # Unter- und Obergrenze rationaler Zahlenraum
irrat_bereich = hoehe/350    # Unter- und Obergrenze irrationaler Zahlenraum
c = (-0.746208973723+0.243562846621j)
z = (-1.58751105596+3.17846606681j)
pygame.init(); pygame.display.set_caption('Julia Set')
screen = pygame.display.set_mode((breite, hoehe))
xoff = yoff = 0
while True:
    print("Rechne...")
    x1 = -rat_bereich - xoff
    x2 = rat_bereich - xoff
    y1 = -irrat_bereich + yoff
    y2 = irrat_bereich + yoff    
    for y in range(hoehe):
        zy = y * (y2 - y1) / (hoehe - 1) + y1
        for x in range(breite):
            zx = x * (x2 - x1) / (breite - 1) + x1
            z = zx + zy * 1j
            for i in range(iterationen):
                if abs(z) > 2.0: break
                z = z * z + c
            screen.set_at((x, y),(i%32*8, i%8*32, int((i*2)%16*16)))
    print("Bereit.\nZoom: [i], [o] zoom\nVerschieben: Cursor-Tasten\nEnde: [q]")
    pygame.display.flip()
    loop = True
    while loop:
        loop = False
        pygame.event.pump(); key = pygame.key.get_pressed()
        if key[pygame.K_q]: exit()   # Programm-Ende
        elif key[pygame.K_i]:
            rat_bereich *= 0.9; irrat_bereich *= 0.9
        elif key[pygame.K_o]:
            rat_bereich *= 1.11111111; irrat_bereich *= 1.11111111
        elif key[pygame.K_UP]:
            yoff=yoff + (irrat_bereich / 25)
        elif key[pygame.K_DOWN]:
            yoff=yoff - (irrat_bereich / 25)
        elif key[pygame.K_LEFT]:
            xoff=xoff - (rat_bereich / 25)
        elif key[pygame.K_RIGHT]:
            xoff=xoff + (rat_bereich / 25)
        else: loop = True