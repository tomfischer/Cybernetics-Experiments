#!/bin/bash

echo "Kybernetik-Experimente"
echo "Medientechnisches Wissen Band 2, Teil 3: Kybernetik (Thomas Fischer)"
echo "Installationsskript für Raspbian und andere Debian-basierte GNU/Linux-Distributionen"

if [ "$EUID" -ne 0 ]
        then echo "Bitte als super user ausführen:"
        echo "sudo bash $0"
        exit
fi

if [[ `uname -a` == *"raspberry"* ]]; then
        echo "Raspbery Pi identifiziert. OK."
else
        distro=$(cat /etc/*-release | grep DISTRIB_ID= | awk -F'=' {'print $2}')

        echo "Dies ist kein Raspbian/Raspberry Pi sondern $distro Linux."
        echo "Python-Skripte einiger Experimente, insbesondere Listing 2.1 (temperature-logger_DE.py), können hier nicht korrekt funktionieren."

        echo -n "Dennoch Installationsversuch unternehmen ? [j/n] "
        read answer
        if [ "$answer" = "${answer#[Jj]}" ] ;then
                echo "OK."
                exit
        fi
fi

sudo apt-get install -y python3-pip
if [ "$?" -ne "0" ]; then
        sudo apt-get install python3-setuptools
        sudo easy_install3 pip
fi

out=$(sudo pip3 show pygame)
if [ -z "$out" ]; then
        echo "installing pygame..."
        if [ "$distro" eq "Kali" ]; then
                sudo apt-get install python3-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsdl1.2-dev libsmpeg-dev python-numpy libportmidi-dev ffmpeg libswscale-dev libavformat-dev libavcodec-dev checkinstall mercurial
                hg clone https://bitbucket.org/pygame/pygame
                python3 setup.py build
                sudo python3 setup.py install
        fi
fi


sudo pip3 install numpy
if [ "$?" -ne "0" ]; then
        sudo apt-get install python-numpy
fi

sudo pip3 install num2words
if [ "$?" -ne "0" ]; then
        sudo apt-get install python3-num2words
fi

sudo pip3 install matplotlib
if [ "$?" -ne "0" ]; then
        sudo apt-get install python-matplotlib
fi

sudo apt-get -y install build-essential
sudo apt-get -y install python-dev
sudo apt-get -y install python3-dev
sudo apt-get -y install portaudio19-dev
sudo apt-get -y install python-setuptools

if [[ `uname -a` == *"raspberry"* ]]; then
        sudo easy_install rpi.gpio
fi

sudo apt-get -y install python3-audio
if [ "$?" -ne "0" ]; then
        sudo apt-get install python-pyaudio
        sudo apt-get install python3-pyaudio
fi

sudo apt-get -y install python3-aubio
sudo apt-get -y install mplayer
sudo apt-get -y install focuswriter
sudo apt-get -y install hunspell-de-de
sudo apt-get -y install hunspell-en-us

if [ -d "Kybernetik-Experimente" ]; then
        echo "Das Verzeichnis 'Kybernetik-Experimente' existiert bereits im aktuellen Pfad."
        exit
fi

echo -n "Kybernetik-Experimente im aktuellen Pfad (${PWD}) installieren? [j/n] "
read answer

if [ "$answer" != "${answer#[Jj]}" ] ;then
        sudo -u $SUDO_USER mkdir Kybernetik-Experimente
        cd Kybernetik-Experimente
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/temperature_logger/temperature-logger_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Turing_Machine/Turing-Machine_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Turing-Machine-Hodges-addition_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Trivial_Machine/trivial-machine_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/liars_paradox/liars-paradox_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/recursive_sentence/numerals_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/transcomputability/twentybytwenty_EN.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/communication_attempt/communication-attempt_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/jar_of_pills/1-jar-of-pills_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/jar_of_pills/many-jars-of-pills_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/chess_board_oscillator/chess-board-oscillator_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Ebbinghaus_forgetting_curve/Ebbinghaus-forgetting-curve_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Eigenform/Eigenform_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/noncontingent_reward/non-condingent-reward_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/vocoder/vocoder_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/vocoder/loving_grace.mp3
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Nontrivial_Machine/nontrivial-machine_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/Julia_set/Julia-set_DE.py
        sudo -u $SUDO_USER wget https://gitlab.com/tomfischer/Cybernetics-Experiments/raw/master/alternants/alternants_DE.py
fi

cd ..
echo "Fertig."
exit
