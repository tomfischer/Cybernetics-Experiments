import numpy
import random

v=[] # Maschinenzustand
lw1=lw2=0
pechstraehne=0
vorhersage=0
computerpunkte=0
spielerpunkte=0

v= numpy.zeros((2,2,2))
vorhersage=random.choice([True, False])
print(chr(27) + "[2J")

while True:
    while True:
        spielerwahl=input("\nDeine Wahl:\n0 oder 1? ")
        if spielerwahl=="1" or spielerwahl=="0":
            break
    spielerwahl=int(spielerwahl)
        
    outTextString = "Deine Wahl: " + str(spielerwahl) + ". Meine Vorhersage war: " + str(int(vorhersage)) + "\n"

    if spielerwahl == vorhersage:
        computerpunkte+=1
        pechstraehne=0
        outTextString = outTextString + "Punkt für mich!"
    else:
        spielerpunkte+=1
        pechstraehne+=1
        outTextString = outTextString + "Ein Punkt für Dich!"

    print(outTextString)
    print("Deine Punkte: " + str(spielerpunkte) + ". Meine Punkte: " + str(computerpunkte))
    print("control+c = Ende")
    if v[lw2, lw1, 0] == spielerwahl:
        v[lw2, lw1, 1] = 1
    else:
        v[lw2, lw1, 1] = 0
    
    v[lw2, lw1, 0] = spielerwahl

    lw2 = lw1
    lw1 = spielerwahl

    if v[lw2, lw1, 1] == 1 and pechstraehne < 2:
        vorhersage = v[lw2, lw1, 0]
    else:
        vorhersage = random.choice([True, False])
