import numpy
import random

v=[] # machine state
lw1=lw2=0
streakofbadluck=0
prediction=0
computerscore=0
playerscore=0

v= numpy.zeros((2,2,2))
prediction=random.choice([True, False])
print(chr(27) + "[2J")

while True:
    while True:
        playerchoice=input("\nPayer choice:\n0 or 1? ")
        if playerchoice=="1" or playerchoice=="0":
            break
    playerchoice=int(playerchoice)
        
    outTextString = "Player choice: " + str(playerchoice) + ". Computer predicted: " + str(int(prediction)) + "\n"

    if playerchoice == prediction:
        computerscore+=1
        streakofbadluck=0
        outTextString = outTextString + "Computer scores!"
    else:
        playerscore+=1
        streakofbadluck+=1
        outTextString = outTextString + "Player scores!"

    print(outTextString)
    print("Player total score: " + str(playerscore) + ". Computer total score: " + str(computerscore))
    print("control+c = End")
    if v[lw2, lw1, 0] == playerchoice:
        v[lw2, lw1, 1] = 1
    else:
        v[lw2, lw1, 1] = 0
    
    v[lw2, lw1, 0] = playerchoice

    lw2 = lw1
    lw1 = playerchoice

    if v[lw2, lw1, 1] == 1 and streakofbadluck < 2:
        prediction = v[lw2, lw1, 0]
    else:
        prediction = random.choice([True, False])
