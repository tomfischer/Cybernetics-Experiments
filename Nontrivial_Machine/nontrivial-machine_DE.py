import random
import copy

# Umfang des Symbolvorrats (sowohl für Inputs als auch für Outputs) abfragen
sv=0
while sv<2 or sv>26:
    sv= int(input("Umfang des Symbolvorrats (2..26)? "))

# Anfänglichen internen Zustand per Zufall auswählen
intstate=int(random.random()*sv)
srain=list(map(chr, range(97, 97+sv)))

sraout=copy.copy(srain) # vorläufiges Output-Array erstellen
random.shuffle(sraout)  # Output-Array durchmischen

print("Akzeptable Inputs / mögliche Outputs: ["+srain[0]+".."+srain[len(srain)-1]+"]")

inp=''
while True:
    inp=input("Die Nicht-Triviale Maschine ist bereit.\nInput (!=Ende)? ")    
    if inp!='':
        if inp=="!": break
        if inp in srain:
            print("Output: " +sraout[(srain.index(inp)+intstate)%sv])
            intstate=(sraout.index(inp)+intstate)%sv
        else: print("Inakzeptables Input-Symbol.")
print("NTM Ende.")