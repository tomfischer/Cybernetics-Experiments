import random
import pygame   # for visualisation

# particle type definitions
VOID = 0
SUBS = 1
LINK = 2
LISU = 3
CATA = 4

class SCL_World():

    # grid size definition (width x height)
    gridsizex=18
    gridsizey=18

    hood=[1,6,2,7,3,8,4,5] # neighborhood structure

    # Illegal bond directions (first entry is a dummy)
    illegalbonds_sbl=[[0,0],[2,4],[1,3],[2,4],[1,3],[1,4],[1,2],[2,3],[3,4]]
    
    # illegal bond angles at l (first entry is a dummy)
    illegalbonds_l = [[0,0],[5,6],[6,7],[7,8],[8,5],[1,4],[1,2],[2,3],[3,4]] 


    grid = []       # 2D array, basic CA data structure
    lockgrid = []   # Allows locking of positions.
    bonds=[]        # bonds between LINK/LISU particles

    # utility functions:

    def all_of(self, kinds): # give me the coordinate pairs of all of these
        c=[]
        for w in kinds:
            for y in range(0, len(self.grid[0])):
                for x in range(0, len(self.grid)):
                    if self.grid[x][y]==w:
                        c.append([x, y])
            random.shuffle(c)
        return c

    def id_neigh(self, xf, yf, index): # identify particle type at index (was: findneigh)
        return self.grid[self.neighcoords(xf, yf, index)[0]][self.neighcoords(xf, yf, index)[1]]
    
    def direction(self, m, other):
        index=-1
        if [other[0]+1, other[1]+1] == m: index=6
        if [other[0], other[1]+1] == m: index=2
        if [other[0]-1, other[1]+1] == m: index=7
        if [other[0]+1, other[1]] == m: index=1
        if [other[0]-1, other[1]] == m: index=3
        if [other[0]+1, other[1]-1] == m: index=5
        if [other[0], other[1]-1] == m: index=4
        if [other[0]-1, other[1]-1] == m: index=8
        return index

    def is_link_bonded(self, x, y):
        i=0
        for b in self.bonds:
            if (b[0]==x and b[1]==y) or (b[2]==x and b[3]==y):
                i+=1
        return i

    def neighcoords(self, xf, yf, index):    # returns coordinates of a neighbour (was: koor)
        if index == 0:      # same
            coords=[xf, yf]
        elif index==1:      # left
            coords=[(xf-1) % self.gridsizex, yf]
        elif index==2:    # up
            coords=[xf, (yf-1) % self.gridsizey]
        elif index==3:    # right
            coords=[(xf+1) % self.gridsizex, yf]
        elif index==4:    # down
            coords=[xf, (yf+1) % self.gridsizey]
        # and the diagonal ones:
        elif index==5:    # left down
            coords=[(xf-1) % self.gridsizex, (yf+1) % self.gridsizey]
        elif index==6:    # left up
            coords=[(xf-1) % self.gridsizex, (yf-1) % self.gridsizey]    
        elif index==7:    # right up
            coords=[(xf+1) % self.gridsizex, (yf-1) % self.gridsizey]
        elif index==8:    # right down
            coords=[(xf+1) % self.gridsizex, (yf+1) % self.gridsizey]
        return coords

    def gridgenerator(self, gridsizex, gridsizey, p):
        gengrid=[]
        for x in range(0, gridsizex):
            line=[]
            for y in range(0, gridsizey):
                if p == [0]:
                    line.append(0)
                else:
                    if random.random()<.75:
                        line.append(p[1])
                    else:
                        line.append(p[0])
            gengrid.append(line)
        return gengrid
        
    def doubleboundlinksinhood(self, l): # bond inhibition field described on top of p. 25
        dblinh=0
        #if not self.is_link_bonded(l[0], l[1]): # if l is a free LINK - commented out: applying the inhibition to both single-bonded and unbonded LINKs
        for i in range(0, 9): # cycle through Moore neighborhood
            if self.is_link_bonded(self.neighcoords(l[0], l[1], i)[0], self.neighcoords(l[0], l[1], i)[1]) == 2:
                dblinh+=1
        #print(dblinh)
        return dblinh # not entirely clean but works : returns 0 when l is bonded (when first condition in this function is in effect)
    
    def illbondsatl(self, l, sbl): # check illegal bond angles (<90deg) at target LINK/LISU
        flag=False
        for b in self.bonds: # looping through bonds
            # consider only bonds connected to l:
            if (b[0]==l[0] and b[1]==l[1]) or (b[2]==l[0] and b[3]==l[1]):
                if (b[0]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][0])[0] and b[1]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][0])[1]):
                    flag=True
                    break
                if (b[2]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][0])[0] and b[3]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][0])[1]):
                    flag=True
                    break
                if (b[0]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][1])[0] and b[1]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][1])[1]):
                    flag=True
                    break
                if (b[2]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][1])[0] and b[3]==self.neighcoords(l[0], l[1], self.illegalbonds_l[sbl][1])[1]):
                    flag=True
                    break
        return flag

    def nocatainhood(self, l):
        flag=0
        for i in range(0,9):
            if self.id_neigh(l[0], l[1], i) == 4: flag=1
        if flag==0:
            return True
        else:
            return False

    def pushLINK(self, mi):    
        # do not do anything if this LINK is bonded
        newgrid = self.grid.copy()
        rebonders=[]
        if self.is_link_bonded(mi[0],mi[1]) == 0:
            q=random.randint(1, 4)        # identify a neighbouring position for LINK to move to
            # 2.31. If location specified by q contains another L, or a C, then take no action.
            n1cds=self.neighcoords(mi[0],mi[1],q)    # coordinates of immediate neighbouring position
        
            # 2.32. If location specified by ni contains an S, the S will be displaced.
            if self.grid[self.neighcoords(mi[0],mi[1], q)[0]][self.neighcoords(mi[0],mi[1], q)[1]] == SUBS:
        
                if not self.pushSUBS(mi, n1cds): # is SUBS cannot be pushed
                    # 2.323. If the S can't be moved into a hole, it will swap with the moving L.
                                    
                    if self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[mi[0]][mi[1]] == 0:
                    
                        # this may not be accurate: LISU should be bonded, and thus immobile
                        if newgrid[mi[0]][mi[1]] == LINK:
                            # should we check here for bonds first? doing so at funtion level
                            newgrid[mi[0]][mi[1]]=SUBS
                            newgrid[n1cds[0]][n1cds[1]]=LINK
                            rebonders.append([n1cds[0], n1cds[1]]) # add moved link to list to attempt re-bonding
                        elif newgrid[mi[0]][mi[1]] == CATA:
                            newgrid[mi[0]][mi[1]]=SUBS
                            newgrid[n1cds[0]][n1cds[1]]=CATA
                        self.lockgrid[n1cds[0]][n1cds[1]]=1
                        self.lockgrid[mi[0]][mi[1]]=1

                        self.grid = newgrid.copy()
                        self.bonding(rebonders) # attempt bonding
                        return True
                        
            # 2.33. If the location specified by q is a hole, then L simply moves into it.
            elif self.grid[self.neighcoords(mi[0], mi[1], q)[0]][self.neighcoords(mi[0], mi[1], q)[1]]==VOID:
                if self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[mi[0]][mi[1]] == 0:                
                    if newgrid[mi[0]][mi[1]] == CATA:
                        newgrid[n1cds[0]][n1cds[1]]=CATA
                        newgrid[mi[0]][mi[1]]=VOID                
                    elif newgrid[mi[0]][mi[1]] != LISU:
                        newgrid[n1cds[0]][n1cds[1]]=LINK
                        rebonders.append([n1cds[0], n1cds[1]]) # add moved link to list to attempt re-bonding
                        newgrid[mi[0]][mi[1]]=VOID
                    self.lockgrid[n1cds[0]][n1cds[1]]=1
                    self.lockgrid[mi[0]][mi[1]]=1
                    
                    self.grid = newgrid.copy()
                    self.bonding(rebonders) # attempt bonding
                    return True
        return False

    def pushSUBS(self, mi, n1cds):
        # identify the neighbouring position's three other neighbours
        n2s=[]
        n2dirs=[]
        newgrid = self.grid.copy()
        for i in range(1, 5): # von 1 bis 4 zaehlen       
            if not self.neighcoords(n1cds[0], n1cds[1], i) == mi:
                n2s.append(self.grid[self.neighcoords(n1cds[0], n1cds[1], i)[0]][self.neighcoords(n1cds[0], n1cds[1], i)[1]])
                n2dirs.append(i)
                                    
        # 2.321. If there is a hole adjacent to the S, it will move into it. If more than one such hole, select randomly.

        # are there voids? if so, then pick one randomly, and push SUBS in there
        vs = [k for k, p in enumerate(n2s) if p == VOID] # find positions of VOIDS in n2s
        ls = [k for k, p in enumerate(n2s) if p == LINK] # find positions of LINKS in n2s

        # 2.321. If there is a hole adjacent to the S, it will move into it. If more than one such hole, select randomly.

        if len(vs)>0:               # pick a VOID for the neighbouring SUBS to move into
            pick=random.choice(vs)

            if self.lockgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0]][self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]] + self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[mi[0]][mi[1]] == 0:

                newgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0]][self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]] = SUBS
                
                # if pusher is LINK:     
                #if newgrid[mi[0]][mi[1]] == LINK: # Wed pm late night change
                if newgrid[mi[0]][mi[1]] == LINK or newgrid[mi[0]][mi[1]] == LISU   :
                        
                    # move link to neighbouring position:
                    newgrid[n1cds[0]][n1cds[1]]=LINK                
                    # leave a VOID behind, if I was a LINK, or leave a SUBS behind if I was a LISU          
                    if newgrid[mi[0]][mi[1]] == LISU:
                        newgrid[mi[0]][mi[1]]=SUBS
                    else:
                        newgrid[mi[0]][mi[1]]=VOID
                        
                # if pusher is CATA:
                elif newgrid[mi[0]][mi[1]] == CATA:
                    # move CATA to neighbouring position
                    newgrid[n1cds[0]][n1cds[1]]=CATA
                    # leave a VOID behind
                    newgrid[mi[0]][mi[1]]=VOID

                #flag all three in lockgrid:
                self.lockgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0]][self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]]=1
                self.lockgrid[n1cds[0]][n1cds[1]]=1
                self.lockgrid[mi[0]][mi[1]]=1
            self.grid=newgrid.copy()
            return True
            
        elif len(ls)>0:                
            # 2.322. If the S can be moved into a hole by passing through bonded links, as in step 1, then it will do so.
            pick=random.choice(ls) # pick a bonded link for the neighbouring SUBS to move into
            # check all 3 positions for lock status:
            if self.lockgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0]][self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]] + self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[mi[0]][mi[1]] == 0:
                
                # new check neineigh LINK is unbonded:
                if self.is_link_bonded(self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0], self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1])>0:
                    
                    # if pusher is LINK:     
                    if self.lockgrid[mi[0]][mi[1]] == LINK:
                        
                        # making it a LISU here:
                        newgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0], self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]] = LISU
                        
                        # move link to neighbouring position:
                        newgrid[n1cds[0], n1cds[1]]=LINK 
                        # leave a VOID behind, if I was a LINK,
                        # or leave a SUBS behind if I was a LISU                
                        if newgrid[mi[0],mi[1]] == LISU:
                            newgrid[mi[0],mi[1]]=SUBS
                        else:
                            newgrid[mi[0],mi[1]]=VOID
                            
                    # if pusher is CATA:
                    elif newgrid[mi[0]][mi[1]] == CATA:
                        newgrid[n1cds[0]][n1cds[1]]=CATA
                        newgrid[mi[0]][mi[1]]=SUBS
                            
                self.lockgrid[self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[0]][self.neighcoords(n1cds[0], n1cds[1], n2dirs[pick])[1]]=1
                self.lockgrid[n1cds[0]][n1cds[1]]=1
                self.lockgrid[mi[0]][mi[1]]=1
            self.grid=newgrid.copy()
            return True
        else:
            return False


    def bondcounter(self, x, y):
        bondcount=0
        for b in self.bonds:
            if (b[0]==x and b[1]==y) or (b[2]==x and b[3]==y):
                bondcount+=1
        return bondcount
    
    def inbonds(self, array):
        for element in self.bonds:
            if [array[0], array[1], array[2], array[3]] == [element[0], element[1], element [2], element [3]]:
                return True
        return False

    def rm(self, m, sbl):
        n=[]
        for a in m:
            if not a==sbl:
                n.append(a)
        return n
    


    # CA process functions:

    def motion1(self):
        # 1.1. Form a list of the coordinates of all holes hi
        # 1.2. For each hi make a random selection, ni, in range 1..4, specifying a neighboring location.
        newgrid = self.grid.copy()
        h=self.all_of([VOID])
        ls=self.all_of([LINK])
        for l in ls:
            if self.is_link_bonded(l[0], l[1])>0:
                h.append(l)

        for hi in h: # including bonded links!
            q=random.randint(1, 4)

            # 1.31 and 1.32 are concurrent qualifications or constraints on motion
            if self.lockgrid[hi[0]][hi[1]]==0 and self.lockgrid[self.neighcoords(hi[0], hi[1], q)[0]][self.neighcoords(hi[0], hi[1], q)[1]]==0:
                found= self.id_neigh(hi[0], hi[1], q)
                
                if found == SUBS:      # it's a substrate
                    # swap them:
                    if self.grid[hi[0]][hi[1]]==LINK:
                        newgrid[hi[0]][hi[1]]=LISU
                    else:
                        newgrid[hi[0]][hi[1]]=SUBS
                    # Should the following be done by setneigh?
                    # No, because setneigh operates on grid, bot newgrid.
                    newgrid[self.neighcoords(hi[0], hi[1], q)[0]][self.neighcoords(hi[0], hi[1], q)[1]]=VOID
                    self.lockgrid[hi[0]][hi[1]]=-1
                    self.lockgrid[self.neighcoords(hi[0], hi[1], q)[0]][self.neighcoords(hi[0], hi[1], q)[1]]=-1
                    
                elif found == LISU:    # it's a link plus substrate
                    # move substrate only if link is bonded
                    if self.is_link_bonded(self.neighcoords(hi[0], hi[1], q)[0],self.neighcoords(hi[0], hi[1], q)[1])>0:
                        # move substrate only:
                        if self.grid[hi[0]][hi[1]]==LINK:
                            newgrid[hi[0]][hi[1]]=LISU                        
                        else:
                            newgrid[hi[0]][hi[1]]=SUBS
                        newgrid[self.neighcoords(hi[0], hi[1], q)[0]][self.neighcoords(hi[0], hi[1], q)[1]]=LINK
                        
                        self.lockgrid[hi[0]][hi[1]]=-1
                        self.lockgrid[self.neighcoords(hi[0], hi[1], q)[0]][self.neighcoords(hi[0], hi[1], q)[1]]=-1

        # 1.4. Bond any moved L, if possible (Rules,6 call that as subroutine when implemented).
        # copy newgrid back to grid:
        self.grid = newgrid.copy()
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0]) # clear lockgrid for locking
        
    def motion2(self):
        # 2.2. For each mi make a random selection, ni, in the range I through 4, specifying a neighboring location.
        for mi in self.all_of([LINK, LISU]):
            self.pushLINK(mi) # go push the LINK
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0]) # clear lockgrid for later rounds of locking

    def motion3(self):
        newgrid = self.grid.copy()
        for ci in self.all_of([CATA]): # throttle this
            if random.uniform(0, 1) > .92:
                q=random.randint(1, 4)
                #3.31. If the location specified by q contains a BL or another K, take no action.
                n1cds=self.neighcoords(ci[0],ci[1],q)    # coordinates of immediate neighbouring position
        
                # identify the neighbouring position's three other neighbours
                n2s=[]
                n2dirs=[]
                for i in range(1, 5): # count from 1 to 4       
                    if not self.neighcoords(n1cds[0], n1cds[1], i) == ci:
                        n2s.append(self.grid[self.neighcoords(n1cds[0], n1cds[1], i)[0]][self.neighcoords(n1cds[0], n1cds[1], i)[1]])
                        n2dirs.append(i)
        
                # 3.32. If the location specified by q contintains a free L, which may be displaced
                # according to the rules of 2.3, then the L will be moved, and the K moved into
                # its place.
                if self.grid[self.neighcoords(ci[0],ci[1], q)[0]][self.neighcoords(ci[0],ci[1], q)[1]] == LINK and self.is_link_bonded(self.neighcoords(ci[0],ci[1], q)[0],self.neighcoords(ci[0],ci[1], q)[1]) == 0:
                # another verision of what we did above
                    self.pushLINK(ci) # go push the LINK
                    
                # 3.33. If the location specified by ni contains an S, then move the S by the
                # rules of 2.32.
                elif self.grid[self.neighcoords(ci[0],ci[1], q)[0]][self.neighcoords(ci[0],ci[1], q)[1]] == SUBS:
                # pretty much what we did in 2 above
        
                    if not self.pushSUBS(ci, n1cds): # if SUBS cannot be pushed
                        if self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[ci[0]][ci[1]] == 0:
                            newgrid[ci[0]][ci[1]]=SUBS
                            newgrid[n1cds[0]][n1cds[1]]=CATA                     
                            self.lockgrid[n1cds[0]][n1cds[1]]=1
                            self.lockgrid[ci[0]][ci[1]]=1
        
                # 3.35. If the location specified by ni is a VOID, the CATA moves into it.
                elif self.grid[self.neighcoords(ci[0],ci[1], q)[0]][self.neighcoords(ci[0],ci[1], q)[1]] == VOID:
                    if self.lockgrid[n1cds[0]][n1cds[1]] + self.lockgrid[ci[0]][ci[1]] == 0:
                        newgrid[n1cds[0]][n1cds[1]]=CATA 
                        newgrid[ci[0]][ci[1]]=VOID
                        self.lockgrid[n1cds[0]][n1cds[1]]=1
                        self.lockgrid[ci[0]][ci[1]]=1   

        self.grid = newgrid.copy()
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0])  # clear lockgrid for locking

    def production(self):
        newgrid = self.grid.copy()
        for ci in self.all_of([CATA]):
            nij=[]
            rebonders=[]
            for u in range(1, 9): 
                if self.id_neigh(ci[0], ci[1], u) == SUBS and self.id_neigh(ci[0], ci[1], self.hood[(self.hood.index(u)+1)%8]) == SUBS:
                    nij.append([u, self.hood[(self.hood.index(u)+1) % 8]])
            # nij established as list of pairs
                
            # produce at some propability level:        
            if len(nij)>0 and random.uniform(0, 1) > 0.2:#.5:#1.7:        
                mat=random.choice(nij)
                #if they are not locked
                if self.lockgrid[self.neighcoords(ci[0],ci[1], mat[0])[0]][self.neighcoords(ci[0],ci[1], mat[0])[1]]+self.lockgrid[self.neighcoords(ci[0],ci[1], mat[1])[0]][self.neighcoords(ci[0],ci[1], mat[1])[1]] == 0:
                    if random.random() > 0.5: # toss a coin
                        newgrid[self.neighcoords(ci[0],ci[1], mat[0])[0]][self.neighcoords(ci[0],ci[1], mat[0])[1]]=LINK
                        rebonders.append([self.neighcoords(ci[0],ci[1], mat[0])[0], self.neighcoords(ci[0],ci[1], mat[0])[1]])
                        newgrid[self.neighcoords(ci[0],ci[1], mat[1])[0]][self.neighcoords(ci[0],ci[1], mat[1])[1]]=VOID
                    else:
                        newgrid[self.neighcoords(ci[0],ci[1], mat[0])[0]][self.neighcoords(ci[0],ci[1], mat[0])[1]]=VOID
                        newgrid[self.neighcoords(ci[0],ci[1], mat[1])[0]][self.neighcoords(ci[0],ci[1], mat[1])[1]]=LINK
                        rebonders.append([self.neighcoords(ci[0],ci[1], mat[1])[0], self.neighcoords(ci[0],ci[1], mat[1])[1]])
                # lock'em up
                self.lockgrid[self.neighcoords(ci[0],ci[1], mat[0])[0]][self.neighcoords(ci[0],ci[1], mat[0])[1]]=1
                self.lockgrid[self.neighcoords(ci[0],ci[1], mat[1])[0]][self.neighcoords(ci[0],ci[1], mat[1])[1]]=1                
        self.grid = newgrid.copy()
        if len(rebonders)>0: # attempt bonding
            self.bonding(rebonders)
            rebonders=[]
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0]) # clear lockgrid for locking

    def disintegration(self):
        newgrid = self.grid.copy()
        for mi in self.all_of([LINK, LISU]): # all_of(): give me the coordinate pairs of all of these
            if random.uniform(0, 1) > .997: #.996:
                #if there is at least one neighbouring VOID!
                # build list of neighbouring VOIDs
                vs=[]
                for u in range(1, 9): 
                    if self.id_neigh(mi[0], mi[1], u) == VOID:
                        vs.append(u)
    
                    random.shuffle(vs)
                    # remove any bonds of this LINK or LISU:   
                    # does the following bond removal belong into bothe following (LINK and LISU) actions?     
                    s=[]
                    for b in self.bonds:
                        if not (b[0]==mi[0] and b[1]==mi[1]) and not (b[2]==mi[0] and b[3]==mi[1]):
                            s.append(b)
                    self.bonds=s
                    
                    # disintegrate and leave SUBS behind:
                    if self.grid[mi[0]][mi[1]] == LINK: # LINK converts SELF and one VOID
                        if len(vs)>0:
                            if (self.lockgrid[mi[0]][mi[1]]+self.lockgrid[self.neighcoords(mi[0], mi[1], vs[0])[0]][self.neighcoords(mi[0], mi[1], vs[0])[1]]==0):                            
                                newgrid[mi[0]][mi[1]]=SUBS
                                newgrid[self.neighcoords(mi[0],mi[1], vs[0])[0]][self.neighcoords(mi[0],mi[1], vs[0])[1]]=SUBS
                                # lock'em up 
                                self.lockgrid[mi[0]][mi[1]]=1
                                self.lockgrid[self.neighcoords(mi[0],mi[1], vs[0])[0]][self.neighcoords(mi[0],mi[1], vs[0])[1]]=1
                                
                    else:      #LISU converts self and two voids # why is the following >1 and not >2???
                        if len(vs) > 2: # LISU converts SELF and two VOIDs
                    
                            if self.lockgrid[mi[0]][mi[1]]+self.lockgrid[self.neighcoords(mi[0],mi[1], vs[0])[0]][self.neighcoords(mi[0],mi[1], vs[0])[1]]+self.lockgrid[self.neighcoords(mi[0],mi[1], vs[1])[0]][self.neighcoords(mi[0],mi[1], vs[1])[1]]==0:
                                newgrid[mi[0]][mi[1]]=SUBS
                                newgrid[self.neighcoords(mi[0],mi[1], vs[0])[0]][self.neighcoords(mi[0],mi[1], vs[0])[1]]=SUBS
                                newgrid[self.neighcoords(mi[0],mi[1], vs[1])[0]][self.neighcoords(mi[0],mi[1], vs[1])[1]]=SUBS
                                # lock'em up 
                                self.lockgrid[mi[0]][mi[1]]=1
                                self.lockgrid[self.neighcoords(mi[0],mi[1], vs[0])[0]][self.neighcoords(mi[0],mi[1], vs[0])[1]]=1
                                self.lockgrid[self.neighcoords(mi[0],mi[1], vs[1])[0]][self.neighcoords(mi[0],mi[1], vs[1])[1]]=1
                        
        self.grid = newgrid.copy()
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0]) # clear lockgrid for another round of locking


    def bond_any(self):
        #bond any
        # Identifying all target LINKs and LISUs
        targetlinks=[]
        for l in self.all_of([LINK, LISU]):
            #if is_link_bonded(l[0],l[1]) == 0: # must be free L!
            targetlinks.append([l[0],l[1]])
        self.bonding(targetlinks)


    def bonding(self, targetlinks):        
        # Illegal bond directions (first entry is a dummy)
        #illegalbonds_sbl=[[0,0],[2,4],[1,3],[2,4],[1,3],[1,4],[1,2],[2,3],[3,4]]

        for l in targetlinks:
            # Walking through list of target LINKs and LISUs  
            n=[] # to collect free LINK/LISU neighbours
            m=[] # to collect singly-bonded LINK/LISU neighbours
            
            # count bonds at l
            lbondcount = self.bondcounter(l[0], l[1])
            # assuming that l (current target LINK/LISU) can bond:
            # targetlcanbond=True # should probably be something like "while len(m+n)>0:"
            # while targetlcanbond: # while lbondcount < 2 and len(m+n)>0:
            
            for u in range(1, 9):
                # findneigh returns neighbouring particle types:
                if self.id_neigh(l[0], l[1], u) == LINK or self.id_neigh(l[0], l[1], u) == LISU:
                    # target LINK/LISU found a neighbouring LINK or LISU
    
                    # if a bond already exists between target LINK/LISU and the identified    
                    # neighbour, then do not pursue this combination further at this step:
                    if not (self.inbonds([l[0], l[1], self.neighcoords(l[0], l[1], u)[0], self.neighcoords(l[0], l[1], u)[1]]) or self.inbonds([self.neighcoords(l[0], l[1], u)[0], self.neighcoords(l[0], l[1], u)[1], l[0], l[1]])):
    
                        flag=0
                        for b in self.bonds: # does bonds contain one or more of my neighours?
                            if (b[0]==self.neighcoords(l[0], l[1], u)[0] and b[1]==self.neighcoords(l[0], l[1], u)[1]) or (b[2]==self.neighcoords(l[0], l[1], u)[0] and b[3]==self.neighcoords(l[0], l[1], u)[1]):
                                flag+=1
                                # fuer jeden bond des gefundenen Nachbarn flag um eins hochzaehlen
                        if flag==0: n=n+[u]#.append(u) # n sind freie (ungebundene) LINK/LISU Nachbarn
                        if flag==1: m=m+[u]#.append(u) # m sind einfach-gebundene LINK/LISU Nachbarn
    
            random.shuffle(m)
            random.shuffle(n)
            m=m+n # append unbound LINKs/LISUs after (prioritised) single-bound LINKs/LISUs
            
            # lets look at each single bonded m and see where their single bond points towards
            while len(m) > 0:
                sbl=m[0] # with n added to m, sbl (single-bonded link) is a misnomer!
                neighlinktarget=[]
                for b in self.bonds: # looping through bonds
                    # checking both possible bond directions:
                    if (b[0]==self.neighcoords(l[0], l[1], sbl)[0] and b[1]==self.neighcoords(l[0], l[1], sbl)[1]):# and inhood(l, [b[2],b[3]])):
                        # print("l, [b[0],b[1]]: "+str(l)+" "+str([b[0],b[1]]))
                        neighlinktarget.append(self.direction(l,[b[2],b[3]])) # direction index relative to l
                        break
                    if (b[2]==self.neighcoords(l[0], l[1], sbl)[0] and b[3]==self.neighcoords(l[0], l[1], sbl)[1]):# and inhood(l, [b[0],b[1]])):
                        # print("l, [b[2],b[3]]: "+str(l)+" "+str([b[2],b[3]]))
                        neighlinktarget.append(self.direction(l,[b[0],b[1]])) # direction index relative to l 
                        break
                
                if self.bondcounter(l[0], l[1]) < 2:
                    flag=0
                    if len(neighlinktarget)==0:
                        if not self.illbondsatl(l, sbl):
                            flag=1
                        
                    elif len(neighlinktarget) < 2:
                        # Next to the target l is a single bonded LINK/LISU bonded to neighlinktarget
                        # and we know where that sbl's bond points
                        if not (self.illegalbonds_sbl[sbl][0]==neighlinktarget[0] or self.illegalbonds_sbl[sbl][1]==neighlinktarget[0]):
                            # previous line checked for illegal angles at sbl.
                            # next line checks for ilegal bond angles at target LINK/LISU
                            if not self.illbondsatl(l, sbl):
                                flag=1
                                
                    # prevent crossed bonds:                    
                    if self.inbonds([self.neighcoords(l[0], l[1], sbl)[0], l[1], l[0], self.neighcoords(l[0], l[1], sbl)[1]]): flag = 0
                    if self.inbonds([l[0], self.neighcoords(l[0], l[1], sbl)[1], self.neighcoords(l[0], l[1], sbl)[0], l[1]]): flag = 0
                    
                    if flag==1 and self.doubleboundlinksinhood(l) < 2 and self.nocatainhood(l): # inhibit bonding near chains of bonded LINKs and bonding near CATAs                 
                        # perform the bond
                        if len(self.bonds) == 0:
                            self.bonds = [[l[0], l[1], self.neighcoords(l[0], l[1], sbl)[0], self.neighcoords(l[0], l[1], sbl)[1]]]
                        else:
                            self.bonds = self.bonds + [[l[0], l[1], self.neighcoords(l[0], l[1], sbl)[0], self.neighcoords(l[0], l[1], sbl)[1]]]
                                            
                # Remove this sbl from the list m regardless of whether a bond was formed or not
                m = self.rm(m, sbl)
                
                for sbl in m:
                    if not self.illbondsatl(l, sbl): # eliminate all neighbouring LINKs/LISUs that would bond with illegal angles
                        m = self.rm(m, sbl)

    def step(self):
        self.motion1()
        self.bond_any()
        self.motion2()
        self.bond_any()
        self.motion3()
        self.production()	# aka composition
        self.disintegration()
        self.bond_any()
        

    def __init__(self):
        self.grid = self.gridgenerator(self.gridsizex, self.gridsizey, [VOID, SUBS])
        self.lockgrid = self.gridgenerator(self.gridsizex, self.gridsizey, [0])
        #self.newgrid = self.grid.copy()

        # set up an initial population:
        cx=int(round(self.gridsizex/2))
        cy=int(round(self.gridsizey/2))

        self.grid[cx][cy]=CATA

        self.grid[cx-1][cy-2]=LINK
        self.grid[cx][cy-2]=LINK
        self.grid[cx+1][cy-2]=LINK

        self.grid[cx+2][cy-1]=LINK
        self.grid[cx+2][cy]=LINK
        self.grid[cx+2][cy+1]=LINK

        self.grid[cx+1][cy+2]=LINK
        self.grid[cx][cy+2]=LINK
        self.grid[cx-1][cy+2]=LINK

        self.grid[cx-2][cy+1]=LINK
        self.grid[cx-2][cy]=LINK
        self.grid[cx-2][cy-1]=LINK

        self.bonds = [[cx-1, cy-2, cx, cy-2], [cx, cy-2, cx+1, cy-2], [cx+1, cy-2, cx+2, cy-1], [cx+2, cy-1, cx+2, cy], [cx+2, cy, cx+2, cy+1], [cx+2, cy+1, cx+1, cy+2], [cx+1, cy+2, cx, cy+2], [cx, cy+2, cx-1, cy+2], [cx-1, cy+2, cx-2, cy+1], [cx-2, cy+1, cx-2, cy], [cx-2, cy, cx-2, cy-1], [cx-2, cy-1, cx-1, cy-2]]


# getting things started:
def main():
    SCLWorld=SCL_World()

    cellsize=25 # the render scale

    # run and visualise the model:
    pygame.init()
    pygame.display.set_caption('SCL World')
    screen =  pygame.display.set_mode((SCLWorld.gridsizex*cellsize, SCLWorld.gridsizey*cellsize))
    clock = pygame.time.Clock()
    while True:
        clock.tick(18) # pretty: 8
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.display.quit()
                    break
            if event.type == pygame.QUIT:
                pygame.display.quit()
                break
        # render the model
        screen.fill((255, 255, 255)) # clear screen
        for b in SCLWorld.bonds: # render bonds first
            if (abs(b[0]-b[2]) < 2) and (abs(b[1]-b[3]) < 2): # don't render wrapping bonds
                pygame.draw.line(screen, (0,0,255), (b[0]*cellsize+int(cellsize/2), b[1]*cellsize+int(cellsize/2)), (b[2]*cellsize+int(round(cellsize/2)), b[3]*cellsize+int(round(cellsize/2))), 3)
        for y in range(SCLWorld.gridsizey):
            for x in range(SCLWorld.gridsizex): # render particles
                if SCLWorld.grid[x][y] == CATA:
                    pygame.draw.circle(screen, (0, 204, 0), (x*cellsize+int(cellsize/2), y*cellsize+int(cellsize/2)),int(cellsize/2-6))
                elif SCLWorld.grid[x][y] == LINK:
                    pygame.draw.rect(screen, (0, 0, 255), (x*cellsize+3, y*cellsize+3, cellsize-9, cellsize-9), 2)
                    pygame.draw.rect(screen, (255, 255, 255), (x*cellsize+5, y*cellsize+5, cellsize-12, cellsize-12))
                elif SCLWorld.grid[x][y] == SUBS:
                    pygame.draw.rect(screen, (255, 0, 0), (x*cellsize+7, y*cellsize+7, cellsize-16, cellsize-16), 3)
                elif SCLWorld.grid[x][y] == LISU:
                    pygame.draw.rect(screen, (0, 0, 255), (x*cellsize+3, y*cellsize+3, cellsize-9, cellsize-9), 2)
                    pygame.draw.rect(screen, (255, 255, 255), (x*cellsize+5, y*cellsize+5, cellsize-12, cellsize-12))
                    pygame.draw.rect(screen, (255, 0, 0), (x*cellsize+7, y*cellsize+7, cellsize-16, cellsize-16), 3)

        SCLWorld.step() # make SCL World do its thing
        pygame.display.flip()

    pygame.display.quit()

if __name__ == '__main__':
    main()
