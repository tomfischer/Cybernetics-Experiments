import random
import copy

# Umfang des Symbolvorrats (sowohl für Inputs als auch für Outputs) abfragen
sv=0
while sv<2 or sv>26:
    sv= int(input("Umfang des Symbolvorrats (2..26)? "))

srain=list(map(chr, range(97, 97+sv)))  # Input-Array generieren
sraout=copy.copy(srain) # vorläufiges Output-Array zu erstellen
random.shuffle(sraout)  # Output-Array durchmischen

print("Akzeptable Inputs und mögliche Outputs: ["+srain[0]+".."+srain[len(srain)-1]+"]")

inp=''
while True:
    print("Die Triviale Maschine ist bereit.")
    inp=input("Input (!=Ende)? ")
    if inp!='':
        if inp=="!": break
        if inp in srain:
            print("Output: " +sraout[srain.index(inp)])
        else:
            print("Inakzeptables Input-Symbol.")
print("TM Ende.")