# alternatives Programm fuer die Tuningmaschine: Addition basierend auf
# Hodges, Andrew (2012): Alan Turing. The Enigma, London, Vintage Books, S. 99

programm = '''
0:+0,+1
1:S+2,+1
2:-3,+2
3:E,L3
'''

# Band wie folgt markieren:
band[15:25] = 1,1,1,1,0,1,1,1,1,1