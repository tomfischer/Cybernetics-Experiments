import pygame

programm = '''
0:+0,1
1:E,L-2
2:S-3,-2
3:S+4,-3
4:+5,+4
5:E,L-2
'''

regeln=[] # Parser
zeilen=programm.split('\n')
for z in zeilen:
    if len(z)>1: regeln.append(z[z.index(':')+1:].split(','))

def schritt(band, regeln, kopf, zustand): # Interpreter
    if regeln[zustand][band[kopf]].find('E') > -1: return band, kopf, zustand
    symbol =- 1
    if regeln[zustand][band[kopf]].find('S') > -1:
        symbol = 1
    elif regeln[zustand][band[kopf]].find('L') > -1:
        symbol = 0
    dk = 0
    if regeln[zustand][band[kopf]].find('-') > -1:
        dk = -1
    elif regeln[zustand][band[kopf]].find('+') > -1:
        dk = 1
    nzust=int(''.join(c for c in regeln[zustand][band[kopf]] if c.isdigit()))
    if symbol >- 1: band[kopf] = symbol
    kopf += dk
    zustand = nzust
    return band, kopf, zustand

pygame.init()
screen = pygame.display.set_mode((800, 50))
pygame.display.set_caption('Demonstration einer Turingmachine')
clock = pygame.time.Clock()
    
band = [0] * 40
band[15:17] = 1,1
zustand = kopf = kopf = 0
run = True
while run:
    clock.tick(3)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.display.quit()
                run = False
        if event.type == pygame.QUIT:
            pygame.display.quit()
            run = False
    screen.fill((255, 255, 255))
    band, kopf, zustand = schritt(band, regeln, kopf, zustand)

    pygame.draw.polygon(screen, (204, 0, 0), [[kopf*20+4, 10], [kopf*20+14, 10], [kopf*20+9, 20]])
    x=0
    for c in band:
        pygame.draw.rect(screen, (0, 0, 0), (x*20+3, 28, 13, 13), 3)
        pygame.draw.rect(screen, (255*int(not c), 255*int(not c), 255*int(not c)), (x*20+3, 28, 13, 13))
        x+=1
    
    pygame.display.flip()
pygame.display.quit()