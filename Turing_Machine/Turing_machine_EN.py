import pygame

programm = '''
0:+0,1
1:E,L-2
2:S-3,-2
3:S+4,-3
4:+5,+4
5:E,L-2
'''

rules=[] # Parser
lines=programm.split('\n')
for z in lines:
    if len(z)>1: rules.append(z[z.index(':')+1:].split(','))

def step(tape, rules, head, state): # Interpreter
    if rules[state][tape[head]].find('E') > -1: return tape, head, state
    symbol =- 1
    if rules[state][tape[head]].find('S') > -1:
        symbol = 1
    elif rules[state][tape[head]].find('L') > -1:
        symbol = 0
    dk = 0
    if rules[state][tape[head]].find('-') > -1:
        dk = -1
    elif rules[state][tape[head]].find('+') > -1:
        dk = 1
    nzust=int(''.join(c for c in rules[state][tape[head]] if c.isdigit()))
    if symbol >- 1: tape[head] = symbol
    head += dk
    state = nzust
    return tape, head, state

pygame.init()
screen = pygame.display.set_mode((800, 50))
pygame.display.set_caption('Turing Machine Demonstration')
clock = pygame.time.Clock()
    
tape = [0] * 40
tape[15:17] = 1,1
state = head = head = 0
run = True

while run:
    clock.tick(2)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.display.quit()
                run = False
        if event.type == pygame.QUIT:
            pygame.display.quit()
            run = False
    screen.fill((255, 255, 255))
    tape, head, state = step(tape, rules, head, state)

    pygame.draw.polygon(screen, (204, 0, 0), [[head*20+4, 10], [head*20+14, 10], [head*20+9, 20]])
    x=0
    for c in tape:
        pygame.draw.rect(screen, (0, 0, 0), (x*20+2, 27, 15, 15), 3)
        pygame.draw.rect(screen, (255*int(not c), 255*int(not c), 255*int(not c)), (x*20+3, 28, 13, 13))
        x+=1
    
    pygame.display.flip()
pygame.display.quit()
