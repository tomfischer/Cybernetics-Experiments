import urllib.request, shutil, os
plrs={"play":"repeat -", "mplayer":"-loop 0", "omxplayer":"--loop", "mpg123": "--loop -1"}
print("Suche Audio-Player...", end='')
for c in plrs:
    if shutil.which(c) != None: break
exit("Kein Audio-Player gefunden.") if c=="" else print(c, "gefunden.")

wort = input("Wort eingeben: ")
if not shutil.os.path.isfile("./"+wort+".mp3"):
    print("Lade Audio-Datei...")
    url="https://www.linguee.de/deutsch-englisch/search?query="+wort
    page = urllib.request.urlopen(url).read().decode('gb2312',errors='ignore')
    page=page[page.index("playSound(this,\"")+16:]
    page=page[:page.index(",")-1]
    soundurl="https://www.linguee.de/mp3/"+page+".mp3"
    # Audio-Datei laden:
    with urllib.request.urlopen(soundurl) as response, open("./"+wort+".mp3", 'wb') as datei:
        data = response.read()
        datei.write(data)
print("control+c = Ende")

os.system("sox ./"+wort+".mp3 tmp.mp3 silence 1 0 1% reverse silence 1 0 1% reverse >/dev/null 2>&1") # Stille abschneiden
os.system("mv ./tmp.mp3 ./"+wort+".mp3 >/dev/null 2>&1")
op = "./" + wort + ".mp3 " + plrs[c] if c == "play" else plrs[c] + " ./"+wort+".mp3 "
os.system(c + " " + op + " >/dev/null 2>&1") # Wiedergabe
# Audio-Datei loeschen?
if input("\nAudio-Datei löschen [j/n]?\n")=="j": os.remove("./"+wort+".mp3")