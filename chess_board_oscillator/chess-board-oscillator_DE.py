import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.animation as animation

def update(data, grid):
  for x in range(16):
    for y in range(16):
      grid[x,y]=np.logical_not(grid[x,y])
  mat.set_data(grid)
  return [mat]

grid = np.kron([[1, 0] * 8, [0, 1] * 8] * 8, np.ones((1, 1)))
animation.rcParams["toolbar"] = "None"
fig, ax = plt.subplots()
fig.canvas.set_window_title("Binär oszillierendes Schachbrett")
mat = ax.matshow(grid, cmap="Greys", interpolation="nearest")
ax.axis("off")
ani = animation.FuncAnimation(fig, update, fargs=(grid, ), interval=300)
plt.show()