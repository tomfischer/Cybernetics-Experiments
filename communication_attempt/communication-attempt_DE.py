import pygame, time
from array import array

pygame.mixer.pre_init(44100, -16, 1, 1024); pygame.init()

class Ton(pygame.mixer.Sound):
    def __init__(self, frq):
        self.frq = frq
        pygame.mixer.Sound.__init__(self, self.synthese())

    def synthese(self):
        p = int(round(pygame.mixer.get_init()[0]/self.frq))
        sampels = array("h", [0] * p)
        amp = 2 ** (abs(pygame.mixer.get_init()[1])-1)-1
        for t in range(p):
            sampels[t] = amp if t < p / 2 else -amp
        return sampels
        
muster=[680, 80, 20498, 8741, 38229, 21056, 0, 1, 32768, 3328, 26, 0, 21504, 248, 0, 6, 7265, 34816, 12826, 12678, 48891, 61312, 0, 4096, 1024, 0, 8192, 4032, 496, 0, 6241, 50720, 8200, 26723, 39675, 61374, 0, 64, 49168, 384, 131, 63, 1543, 49164, 16, 2052, 4144, 4120, 24640, 3139, 1, 38912, 12556, 390, 1028, 2052, 4120, 1088, 12296, 32800, 8320, 32896, 32774, 192, 12292, 30080, 2056, 16, 31744, 8564, 46596, 58622, 57793, 47106, 33714, 1287, 58378, 3080, 13824, 0, 56, 8192, 29973, 21728, 2720, 20, 15, 32768, 32704, 896, 57356, 96, 13313, 24780, 1633, 5140, 16932, 18560, 17680, 132, 8193, 64, 148, 121, 62704]
        
l = Ton(frq=261)	# c'
m = Ton(frq=392)	# g'
h = Ton(frq=523)	# c''
seq=''
intbin = lambda x, n: format(x,'b').zfill(n)
for n in muster: seq+=intbin(n, 16)
    
m.play(-1); time.sleep(15); m.stop()
curr=-1; accu=0; n=0
for t in seq:
    if n<1679:
        if t==curr: accu+=1
        else:
            if t=='1':
                l.play(-1); time.sleep(.1*accu); l.stop()
            else:
                h.play(-1); time.sleep(.1*accu); h.stop()
            accu=1
            curr=t
    n+=1
    
m.play(-1); time.sleep(15); m.stop()