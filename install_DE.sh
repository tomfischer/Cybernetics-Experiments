#!/bin/bash

echo "Kybernetik-Experimente"
echo "Installationsskript für Raspberry Pi"

if [ "$EUID" -ne 0 ]
	then echo "Bitte als super user ausführen:"
	echo "sudo bash $0"
	exit
fi

if [[ `uname -a` == *"raspberry"* ]]; then
	echo "Raspbery Pi identifiziert. OK."
else
	echo "Dies ist kein Raspberry Pi."
	echo "Bitte lesen Sie sie Anweisungen unter"
	echo "https://gitlab.com/tomfischer/Cybernetics-Experiments/blob/master/README.md"
	exit
fi

sudo apt-get install -y python3-pip
sudo pip3 install numpy
sudo pip3 install num2words
sudo pip3 install matplotlib
sudo pip3 install Splipy
sudo apt-get -y install build-essential
sudo apt-get -y install python-dev
sudo apt-get -y install python3-dev
sudo apt-get -y install portaudio19-dev
sudo apt-get -y install python-setuptools
sudo easy_install rpi.gpio
sudo apt-get -y install python3-audio
sudo apt-get -y install python3-aubio
sudo apt-get -y install mplayer
sudo apt-get -y install focuswriter
sudo apt-get -y install hunspell-de-de
sudo apt-get -y install hunspell-en-us

if [ -d "Cybernetics-Experiments" ]; then
	echo "Das Verzeichnis 'Cybernetics-Experiments' existiert bereits im aktuellen Pfad."
	exit
fi

while true; do
	read -p "Kybernetik-Experimente im aktuellen Pfad (${PWD}) installieren? [j/n] " jn
		case $jn in
			[Jj]* ) sudo -u $SUDO_USER git clone https://gitlab.com/tomfischer/Cybernetics-Experiments.git; break;;
			[Nn]* ) exit;;
			* ) echo "Bitte antworten Sie mit Ja oder Nein.";;
		esac
done
