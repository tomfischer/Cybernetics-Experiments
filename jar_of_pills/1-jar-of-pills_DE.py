import math, random
from collections import Counter
print(chr(27) + "[2J")
print("X = hell rund\nx = hell länglich\nO = dunkel rund\no = dunkel länglich\n") 

def lz_complexity(s): # Algorithmus nach Kaspar & Schuster 1987, S. 843, Fig. 1
    i, k, l, k_max = 0, 1, 1, 1
    n = len(s) - 1
    c = 1
    while True:
        if s[i + k - 1] == s[l + k - 1]:
            k = k + 1
            if l + k >= n - 1:
                c = c + 1
                break
        else:
            if k > k_max: k_max = k
            i = i + 1
            if i == l:
                c = c + 1; l = l + k_max
                if l + 1 > n: break
                else:
                    i = 0; k = 1; k_max = 1
            else:
                k = 1
    return c

pillenzahl = 100
vorpillen = []

for i in range(0,pillenzahl):
    farbe = bool(random.getrandbits(1))
    pille = 'x' if i < pillenzahl/2 else 'o'
    if farbe == 0: pille = pille.upper()
    vorpillen.append(pille)

shake=u'SCHÜTTELN!'#.decode('utf-8')
vorfarben = vorformen = nachfarben = nachformen = ''
nachpillen = list(vorpillen)
random.shuffle(nachpillen)
print('      vor                        nach\n')
for i in range(0,pillenzahl):
    print(vorpillen[i]+' ', end = '')
    if (i % 10) == 9:
        print('    '+shake[int(i/10)]+'     ', end = '')
        for j in range(0,10):
            print(nachpillen[int(i/10)*10+j]+' ', end = '')
            if nachpillen[int(i/10)*10+j]=='O' or nachpillen[int(i/10)*10+j]=='o':
                nachfarben+='1'
            else:
                nachfarben+='0'
            if nachpillen[int(i/10)*10+j]=='o' or nachpillen[int(i/10)*10+j]=='x':
                nachformen+='1'
            else:
                nachformen+='0'
        print()
    vorfarben+='1' if vorpillen[i] == 'O' or vorpillen[i] == 'o' else '0'
    vorformen+='1' if vorpillen[i] == 'o' or vorpillen[i] == 'x' else '0'

print('\n'+u'\u0394'+' Komplexität der Farb-Verteilung:',lz_complexity(nachfarben)-lz_complexity(vorfarben))
print(u'\u0394'+' Komplexität der Form-Verteilung:',lz_complexity(nachformen)-lz_complexity(vorformen),'\n')