import math, random
from collections import Counter
print(chr(27) + "[2J")
print("X = bright round\nx = bright oval\nO = dark round\no = dark oval\n")

def lz_complexity(s): # algorithm based on Kaspar & Schuster 1987, S. 843, Fig. 1
    i, k, l, k_max = 0, 1, 1, 1
    n = len(s) - 1
    c = 1
    while True:
        if s[i + k - 1] == s[l + k - 1]:
            k = k + 1
            if l + k >= n - 1:
                c = c + 1
                break
        else:
            if k > k_max: k_max = k
            i = i + 1
            if i == l:
                c = c + 1; l = l + k_max
                if l + 1 > n: break
                else:
                    i = 0; k = 1; k_max = 1
            else:
                k = 1
    return c

no_of_pills = 100
pills_before = []

for i in range(0, no_of_pills):
    farbe = bool(random.getrandbits(1))
    pill = 'x' if i < no_of_pills/2 else 'o'
    if farbe == 0: pill = pill.upper()
    pills_before.append(pill)

shake=u'S H A K E '#.decode('utf-8')
colours_before = forms_before = colours_after = afterforms = ''
pills_after = list(pills_before)
random.shuffle(pills_after)
print('     before                       after\n')
for i in range(0, no_of_pills):
    print(pills_before[i]+' ', end = '')
    if (i % 10) == 9:
        print('    ' + shake[int(i/10)]+'     ', end = '')
        for j in range(0,10):
            print(pills_after[int(i/10)*10+j] + ' ', end = '')
            if pills_after[int(i/10)*10+j] == 'O' or pills_after[int(i/10)*10+j] == 'o':
                colours_after += '1'
            else:
                colours_after+='0'
            if pills_after[int(i/10)*10+j] == 'o' or pills_after[int(i/10)*10+j] == 'x':
                afterforms += '1'
            else:
                afterforms += '0'
        print()
    colours_before += '1' if pills_before[i] == 'O' or pills_before[i] == 'o' else '0'
    forms_before += '1' if pills_before[i] == 'o' or pills_before[i] == 'x' else '0'

print('\n'+u'\u0394'+' Complexity of colour distribution:',lz_complexity(colours_after)-lz_complexity(colours_before))
print(u'\u0394'+' Complexity of shape distribution:',lz_complexity(afterforms)-lz_complexity(forms_before),'\n')
