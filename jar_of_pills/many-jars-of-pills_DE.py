import math, random #, string, sys, fileinput
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

col_comp_deltas=[]
sha_comp_deltas=[]

jars=25
pillenzahl=100
    
def lz_complexity(s): # Algoritmus nach Kaspar & Schuster 1987, S. 843, Fig. 1
    i, k, l = 0, 1, 1
    k_max = 1
    n = len(s) - 1
    c = 1
    while True:
        if s[i + k - 1] == s[l + k - 1]:
            k = k + 1
            if l + k >= n - 1:
                c = c + 1
                break
        else:
            if k > k_max:
                k_max = k
            i = i + 1
            if i == l:
                c = c + 1
                l = l + k_max
                if l + 1 > n:
                    break
                else:
                    i = 0
                    k = 1
                    k_max = 1
            else:
                k = 1
    return c
    
for q in range(0,jars):  

    beforepills=[]
    
    for i in range(0,pillenzahl):
        color=bool(random.getrandbits(1))
        if i<pillenzahl/2:
            pill='x'
        else:
            pill='o'
        if color==0:
            pill=pill.upper()
        beforepills.append(pill)
    
    shake='!SHAKING!!'
    beforecolors=''
    beforeshapes=''
    aftercolors=''
    aftershapes=''
    afterpills=list(beforepills)
    random.shuffle(afterpills)
    
    print('      BEFORE                         AFTER  \n')
    #print('')
    
    for i in range(0,pillenzahl):
        print(beforepills[i],)
        if (i % 10)==9:
            print('   ',shake[int(i/10)],'   ',)
    
            for j in range(0,10):
                print(afterpills[int(i/10)*10+j],)
    
                if afterpills[int(i/10)*10+j] == 'O' or afterpills[int(i/10)*10+j] == 'o':
                    aftercolors+='1'
                else:
                    aftercolors+='0'
                if afterpills[int(i/10)*10+j] == 'o' or afterpills[int(i/10)*10+j] == 'x':
                    aftershapes+='1'
                else:
                    aftershapes+='0'
            print('')
    
        if beforepills[i] == 'O' or beforepills[i] == 'o':
            beforecolors+='1'
        else:
            beforecolors+='0'
        if beforepills[i] == 'o' or beforepills[i] == 'x':
            beforeshapes+='1'
        else:
            beforeshapes+='0'
    
    print('')
    print(u'\u0394'+' complexity of (color distribution):',lz_complexity(aftercolors)-lz_complexity(beforecolors))
    print(u'\u0394'+' complexity of (shape distribution):',lz_complexity(aftershapes)-lz_complexity(beforeshapes))
    
    print('')
    col_comp_deltas.append(lz_complexity(aftercolors)-lz_complexity(beforecolors))
    sha_comp_deltas.append(lz_complexity(aftershapes)-lz_complexity(beforeshapes))

# create plot
fig, ax = plt.subplots()
index = np.arange(jars)
bar_width = 0.35
opacity = 0.8
 
rects1 = plt.bar(index, col_comp_deltas, bar_width,
                 alpha=opacity,
                 color='gray',
                 label='Farbe')
 
rects2 = plt.bar(index + bar_width, sha_comp_deltas, bar_width,
                 alpha=opacity,
                 color='black',
                 label='Form')
 
plt.xlabel('Gläser mit Pillen'.decode('utf-8'))
plt.ylabel('delta Komplexität nach Schütteln'.decode('utf-8'))
plt.title('Komplexität bezüglich Farbe und Form'.decode('utf-8'))
plt.xticks(index + bar_width, range(1,jars+1))
plt.legend(bbox_to_anchor=(1, 0.85),
           bbox_transform=plt.gcf().transFigure)
 
#plt.tight_layout()
plt.show()
