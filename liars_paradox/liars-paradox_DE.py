def auswertung(aussage):
    if aussage == "Ich bin ein Lügner.":
        aussage=aussage.replace('ein', 'kein')
        return aussage
    if aussage == "Ich bin kein Lügner.":
        aussage=aussage.replace('kein', 'ein')
        return aussage
    
aussage="Ich bin ein Lügner."
vorherige_aussage=""

while not vorherige_aussage == aussage:
    vorherige_aussage=aussage
    aussage=auswertung(aussage)
    print(aussage)
