def evaluate(proposition):
    if proposition == "I am a liar.":
        proposition=proposition.replace(' a ', ' not a ')
        return proposition
    if proposition == "I am not a liar.":
        proposition=proposition.replace(' not a ', ' a ')
        return proposition

proposition="I am a liar."
previous_proposition=""

while not previous_proposition == proposition:
    previous_proposition=proposition
    proposition=evaluate(proposition)
    print(proposition)