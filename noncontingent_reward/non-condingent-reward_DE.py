import math
import random

total_runden=25 # Anzahl der zu präsentierenden Zahlenpaare
paare=[]
k=2.197225*(1/total_runden)
print(chr(27) + "[2J" + "Studie zur Fähigkeit der Mustererkennung")
input("\n\n\n\n        zum Start [Enter] drücken\n\n\n\n\n\n")
runde=0
while runde<total_runden:
    print(chr(27) + "[2J")
    print("\n\n\n\nFrage " + str(runde+1) + " von " + str(total_runden) + ":")
    print("Passen die folgenden zwei Zahlen zusammen?\n\n")
    while True:
        a=random.randint(0, 100)
        while True:
            b=random.randint(0, 100)
            if not a==b: break
        if not ([a,b] in paare or [b,a] in paare): break
    print("                "+str(a)+"      "+str(b))
    while True:
        aw=input("\n\n[j]a oder [n]ein? ")
        if aw=="j" or aw=="n": break
    if random.uniform(0.07,0.7) > 0.1*math.exp(k*runde):
        print("\n\n\n\nInkorrekt. "+str(a) + " und " + str(b) + " passen", end='')
        if aw=="j": print(" nicht", end='')
        print(" zusammen.")
    else:
        print("\n\n\n\nKorrekt! " + str(a) + " und " + str(b) + " passen", end='')
        if aw=="n": print(" nicht", end='')
        print(" zusammen.")
    input("\n\n             weiter mit [Enter]\n\n")
    runde+=1