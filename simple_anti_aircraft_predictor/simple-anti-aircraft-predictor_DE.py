import pygame
from sys import exit
from numpy import *

class Plane:
    def __init__(self):
        self.breite = 800; self.hoehe = 600
        pygame.init()
        self.screen = pygame.display.set_mode((self.breite, self.hoehe))
        self.screen.fill((255, 255, 255))
        self.mouse_pos = pygame.mouse.get_pos()
        pygame.display.set_caption('Einfacher Luftabwehr-Prediktor')
        self.draw_color = (0,0,0)
        pygame.draw.line(self.screen, (0,0,0), [self.hoehe,0], [self.hoehe,self.hoehe], 1)
        pygame.display.update()

    def event_loop(self):
        running = True
        drawing = False
        old = [-999,-999]
        x = []; y = []
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running=False
                    exit()
                if event.type ==  pygame.MOUSEBUTTONDOWN:
                    if old==[-999,-999]:
                        self.screen.fill((255, 255, 255))
                        pygame.draw.line(self.screen,(0,0,0),[600,0],[600,600], 1)
                        pygame.display.update()
                        old=pygame.mouse.get_pos()
                        flag=0
                    drawing = True
                if event.type == pygame.MOUSEBUTTONUP:
                    drawing = False
                    old = [-999,-999]
                    x = []; y = []
            if drawing:
                if pygame.mouse.get_pos()[0]<600:
                    x.append(pygame.mouse.get_pos()[0])
                    y.append(pygame.mouse.get_pos()[1])
                elif flag==0 and pygame.mouse.get_pos()[0]>599:
                    extrap = polyfit(x, y, 2)
                    for n in [605, 650, 700, 750, 795]:
                        pygame.draw.circle(self.screen, (255,0,0),  (n, int(polyval(extrap, n))), 5)
                    flag = 1
                if old != [-999,-999]:
                    pygame.draw.line(self.screen, (0,0,0), old, pygame.mouse.get_pos(), 2)
                    old = pygame.mouse.get_pos()
                    pygame.display.update()

if __name__ == '__main__':
    p = Plane()
    p.event_loop()