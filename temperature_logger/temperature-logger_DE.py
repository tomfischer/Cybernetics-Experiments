import os, glob, time
import matplotlib as mpl
mpl.use('tkagg')
import matplotlib.pyplot as plt

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
fig, ax = plt.subplots()
fig.canvas.set_window_title('Temperatur-Logger')
ax.axis([0,300,15,70])  # Dimensionierung des Koordinatensystems

def read_temp():
    while True:
        f=open(glob.glob('/sys/bus/w1/devices/'+'28*')[0]+'/w1_slave', 'r')
        lines = f.readlines()
        f.close()
        if lines[0].strip()[-3:] != 'YES\n': break
        time.sleep(0.2)
    equal=lines[1].find('t=')
    if equal != -1: return float(lines[1][equal+2:]) / 1000

i = 0
while True:
    plt.scatter(i,read_temp(),color='red',s=2)
    i += 1
    plt.pause(1)  # Pause zwischen Messungen in Sekunden