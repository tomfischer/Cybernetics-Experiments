import itertools, pygame, time

gridx = 20
gridy = 20

xarr = yarr = []
tm=time.time()
pygame.init()
pygame.display.set_caption(str(gridx) +' x ' + str(gridy))
screen = pygame.display.set_mode((gridx*10, gridy*10))

for i in itertools.product([0,1],repeat=gridx*gridy):
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:pygame.display.quit()
        if event.type == pygame.QUIT: pygame.display.quit()   
    try:
        if i.index(1) < l:
            l=i.index(1)
            print("Binary digit: " + str(400-l) + " of " + str(gridx * gridy))
            print("Time elapsed: " + str(time.time()-tm))
    except ValueError: l = gridx * gridy
    screen.fill((0, 0, 0))        
    j=[i[x:x+gridx] for x in range(0, len(i), gridy)]
    for x in range(gridx):
        for y in range(gridy):
            if j[y][x]==0:
                pygame.draw.rect(screen,(255, 255, 255),(x*10, y*10, 10, 10), 0)
    pygame.display.flip()
pygame.display.quit()