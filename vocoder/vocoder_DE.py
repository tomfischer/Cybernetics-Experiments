import sys
from aubio import source, sink, pvoc

samplerate = 44100
f = source(sys.argv[1], samplerate, 256)
g = sink(sys.argv[2], samplerate)
total_frames, read = 0, 256
pv = pvoc(512, 256)

while read:
    samples, read = f()
    spectrum = pv(samples)
    #spectrum.norm *= .8             # ggf. Output-Amplitude anpassen
    spectrum.phas[:] = 0.
    new_samples = pv.rdo(spectrum)
    g(new_samples, read)
    total_frames += read