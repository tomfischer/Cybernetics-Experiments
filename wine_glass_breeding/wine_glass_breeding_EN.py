import matplotlib
matplotlib.use('TkAgg')
from mpl_toolkits.mplot3d import  axes3d,Axes3D
import matplotlib.pyplot as plt
from splipy import *
import splipy.curve_factory as curves
import splipy.surface_factory as surfaces
from random import uniform as rn
import random
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib.widgets import Slider, Button, RadioButtons, CheckButtons
import tkinter
import sys

class E(tkinter.Tk):
    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.protocol("WM_DELETE_WINDOW", self.dest)
        self.main()

    def dest(self):
        self.destroy()
        sys.exit()

    def rndna(self): # produce random DNA
        return [rn(0.45, 0.52), rn(0.034, 0.09), rn(0.034, 0.09), rn(0.034, 0.09), rn(0.035, 0.09), rn(0.3, 1), rn(0.3, 1), rn(0.3, 1), rn(0, 0.2), rn(0.4, 0.8), rn(0.4, 0.8)]

    def surface(self, dna): # surface modeling
        cup=curves.bezier([[0, dna[7], dna[10]+dna[9]+dna[8]], [0, dna[6], (dna[10]-dna[9])*2/3+dna[9]+dna[8]], [0, dna[5], (dna[10]-dna[9])/3+dna[9]+dna[8]], [0, dna[4], dna[9]]], quadratic=False, relative=False)
        neck=curves.bezier([[0, dna[4], dna[9]+dna[8]], [0, dna[3], (dna[9]-dna[8])*2/3+dna[8]], [0, dna[2], (dna[9]-dna[8])/3+dna[8]], [0, dna[1], dna[8]]], quadratic=False, relative=False)
        foot=curves.line((0, dna[1], dna[8]), (0, dna[0], 0))
        cup.append(neck)
        cup.append(foot)
        surf = surfaces.revolve(cup, axis=[0,0,1])
        u = np.linspace(surf.start(0), surf.end(0), 24)
        v = np.linspace(surf.start(1), surf.end(1), 50)
        return surf(u,v)

    def onclick(self, event, selected, ax):
        for n in range(0,9):
            if event.artist == ax[n]:
                if not n in selected:
                    selected[0], selected[1]=selected[1], n
        for n in range(0,9):
            if n in selected:
                ax[n].set_facecolor((0.82, 0.82, 0.82))
            else: ax[n].set_facecolor((1, 1, 1))
        self.canvas.flush_events()
        self.canvas.draw()

    def breed(self, s, ax, dnarray):
        if s[0] == None or s[1] == None : return
        newdnarray=dnarray
        for index in [n*3+m for m in range(0,3) for n in range(0,3)]:
            help, altgenes=[], self.rndna()
            for g in range(11): # splice and mutate:
                help.append(dnarray[s[random.randint(0, 1)]][g])
                if random.random() > 0.9: help[g]=altgenes[g]
            newdnarray[index]=help
            ax[index].set_facecolor((1, 1, 1))
        dnarray=newdnarray
        s[0], s[1]=None, None
        ax=self.populate(ax, dnarray) #self.canvas.flush_events()
        self.canvas.draw()

    def populate(self, ax, dnarray):
        for index in [n*3+m for m in range(0,3) for n in range(0,3)]:
            if ax[index] != None: ax[index].remove()
            ax[index] = self.fig.add_subplot(3, 3, index+1, projection='3d')
            if dnarray[index] == None: dnarray[index]=self.rndna()
            x=self.surface(dnarray[index])
            ax[index].plot_surface(x[:,:,0], x[:,:,1], x[:,:,2], color=(0.7, 0.7, 0.7), rstride=1, cstride=1, linewidth=0, antialiased=True)
            ax[index].view_init(20, 0)
            plt.axis('off')
            ax[index].set_picker(True)

    def main(self):
        selected, dnarray=[None]*2, [None]*9
        self.fig = plt.figure(figsize=(6,6))
        self.frame = tkinter.Frame(self)
        self.frame.pack()
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.frame)
        self.canvas.mpl_connect('pick_event', lambda event: self.onclick(event, selected, ax))
        self.canvas._tkcanvas.pack(side='top', fill='both', expand=1)
        ax=[None]*9
        self.populate(ax, dnarray)
        self.fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
        self.btn = tkinter.Button(self,text='select two parents above, then click here to breed a new generation', command = lambda: self.breed(selected, ax, dnarray))
        self.btn.pack()

if __name__ == "__main__":
    app = E(None)
    app.title('Wine Glass Breeding')
    app.mainloop()
